import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConversionAddComponent } from './Conversion/add-conversion/conversion-add.component';
import { ConversionEditComponent } from './Conversion/edit-conversion/conversion-edit.component';
import { ConversionGetComponent } from './Conversion/get-conversion/conversion-get.component';

const routes: Routes = [
  {
  path: 'conversion/create',
  component: ConversionAddComponent
},
{
  path: 'conversion/edit/:id',
  component: ConversionEditComponent
},
{
  path: 'conversion',
  component: ConversionGetComponent
},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
