export default class Respuesta {
    statusCode: Number;
    original_value: String;
    original_unit: String;
    convert_value: String;
    convert_unit: String;
  }