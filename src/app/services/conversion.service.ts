import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConversionService {

  uri = 'https://moywt6nti0.execute-api.us-east-1.amazonaws.com/production/conversion';

  constructor(private http: HttpClient) { }

  addConversion(valor ,unidad ,convertira ) {
    var url = this.uri+"?valor="+valor+"&unidad=\""+unidad+"\"&convertira=\""+convertira+"\"";
    return this.http
    .get(url);
  }

  getConversion() {
    return this
           .http
           .get(`${this.uri}`);
  }

  editConversion(id) {
    return this
            .http
            .get(`${this.uri}/${id}`);
    }

  updateConversion(valor ,unidad ,convertira , id) {

    const obj = {
      valor:valor,
unidad:unidad,
convertira:convertira

      };
    this
      .http
      .put(`${this.uri}/${id}`, obj)
      .subscribe(res => console.log('Done'));
  }

 deleteConversion(id) {
    return this
              .http
              .delete(`${this.uri}/${id}`);
  }
}
