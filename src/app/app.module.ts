import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { AppComponent } from './app.component';
import { ConversionAddComponent } from './Conversion/add-conversion/conversion-add.component';
import { ConversionEditComponent } from './Conversion/edit-conversion/conversion-edit.component';
import { ConversionGetComponent } from './Conversion/get-conversion/conversion-get.component';

import { ToastrModule } from 'ng6-toastr-notifications';
import { ConversionService } from './services/conversion.service';


@NgModule({
  declarations: [
    AppComponent,
ConversionAddComponent,
ConversionGetComponent,
ConversionEditComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    ToastrModule.forRoot()
  ],
  providers: [ 
    ConversionService
 
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
