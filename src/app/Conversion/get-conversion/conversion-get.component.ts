import { Component, OnInit } from '@angular/core';
import Conversion from '../../models/Conversion';
import { ConversionService } from '../../services/conversion.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-conversion-get',
  templateUrl: './conversion-get.component.html',
  styleUrls: ['./conversion-get.component.css']
})

export class ConversionGetComponent implements OnInit {

  conversion: Conversion[];

  constructor( private bs: ConversionService, public toastr: ToastrManager) {}

  ngOnInit() {
    this.bs
      .getConversion()
      .subscribe((data: Conversion[]) => {
        this.conversion = data;
    });
  }

  deleteConversion(id) {
    this.bs.deleteConversion(id).subscribe(res => {
      console.log('Deleted');
      this.bs
      .getConversion()
      .subscribe((data: Conversion[]) => {
        this.conversion = data;
    });
      this.showDeleted();
    });
  }
  showSuccess() {
    this.toastr.successToastr('This is success toast.', 'Success!');
}

showDeleted() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}

showWarning() {
    this.toastr.warningToastr('This is warning toast.', 'Alert!');
}

showInfo() {
    this.toastr.infoToastr('This is info toast.', 'Info');
}



showToast(position: any = 'top-left') {
    this.toastr.infoToastr('This is a toast.', 'Toast', {
        position: position
    });
}
}

