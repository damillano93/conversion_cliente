import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

import { ConversionService } from '../../services/conversion.service';

import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-conversion-edit',
  templateUrl: './conversion-edit.component.html',
  styleUrls: ['./conversion-edit.component.css']
})
export class ConversionEditComponent implements OnInit {
  
  angForm: FormGroup;
  conversion: any = {};

  constructor(private route: ActivatedRoute,
    
    private router: Router,
    public toastr: ToastrManager,
    private bs: ConversionService,
    private fb: FormBuilder) {
      this.createForm();
     }

  createForm() {
    this.angForm = this.fb.group({
        valor: ['', Validators.required ],
unidad: ['', Validators.required ],
convertira: ['', Validators.required ]

      });
    }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.bs.editConversion(params['id']).subscribe(res => {
        this.conversion = res;
      });
    });
     
  }
showInfo() {
  this.toastr.infoToastr('El registro fue actualizado', 'Actualizado');
}
  updateConversion(valor, unidad, convertira ) {
   this.route.params.subscribe(params => {
      this.bs.updateConversion(valor, unidad, convertira  ,params['id']);
      this.showInfo()
      this.router.navigate(['conversion']);
   });
}
}
