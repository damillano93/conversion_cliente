import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import Respuesta from '../../models/Respuesta';
import { ConversionService } from '../../services/conversion.service';

import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
  selector: 'app-conversion-add',
  templateUrl: './conversion-add.component.html',
  styleUrls: ['./conversion-add.component.css']
})
export class ConversionAddComponent implements OnInit {
  respuesta: Respuesta;
  angForm: FormGroup;
  constructor(private fb: FormBuilder, private Conversion_ser: ConversionService, public toastr: ToastrManager) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
       valor: ['', Validators.required ],
unidad: ['', Validators.required ],
convertira: ['', Validators.required ]

    });
  }
  showSuccess() {
    this.toastr.successToastr('El registro fue creado.', 'Creado!');
}

 showError() {
    this.toastr.errorToastr('El registro fue borrado correctamente', 'Borrado');
}
  addConversion(valor, unidad, convertira ) {
    this.Conversion_ser.addConversion(valor, unidad, convertira ).subscribe((data: Respuesta) => {
      this.respuesta = data;
      console.log(data)
  });
    this.angForm.reset();

    this.showSuccess();
  }

  ngOnInit() {
    this.respuesta;

  }

}
